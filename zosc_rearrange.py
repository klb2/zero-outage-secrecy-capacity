import numpy as np
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
import rearrangement_algorithm as ra

def sec_capac(snr_bob, snr_eve):
    return np.maximum(np.log2(1+snr_bob) - np.log2(1+snr_eve), 0)

def zosc_rearrange(num_bob, num_eve, mu, num_steps=1000):
    """
    Arguments
    ---------
    num_bob : int
        Number of antennas at Bob

    num_eve : int
        Number of antennas at Eve

    mu : float
        Mean of the channel gains to Eve

    num_steps : int
        Number of quantization steps in the RA
    """
    dist_bob = stats.expon(scale=1)
    dist_eve = stats.expon(scale=mu)
    qf_bob = [dist_bob.ppf]*num_bob
    qf_eve = [dist_eve.ppf]*num_eve
    #qf_eve = [lambda p: -dist_eve.ppf(1-p)]*num_eve
    ra_bob_lower, ra_bob_upper = ra.bounds_VaR(0., qf_bob, num_steps=num_steps, method="upper")
    ra_eve_lower, ra_eve_upper = ra.bounds_VaR(0., qf_eve, num_steps=num_steps, method="upper")
    #ra_eve_comon = ra.create_comonotonic_ra(0., qf_eve, num_steps)
    #ra_eve_lower, ra_eve_upper = (0, ra_eve_comon[0]), (0, ra_eve_comon[1])
    sum_bob_lower = np.sum(ra_bob_lower[1], axis=1)
    sum_bob_upper = np.sum(ra_bob_upper[1], axis=1)
    sum_eve_lower = np.sum(ra_eve_lower[1], axis=1)
    sum_eve_upper = np.sum(ra_eve_upper[1], axis=1)
    sec_capac_lower = sec_capac(np.sort(sum_bob_lower), np.sort(sum_eve_upper))
    sec_capac_upper = sec_capac(np.sort(sum_bob_upper), np.sort(sum_eve_lower))
    zosc_lower = np.min(sec_capac_lower)
    zosc_upper = np.min(sec_capac_upper)
    return zosc_lower, zosc_upper

def main(num_bob, num_eve, mu, num_steps=1000, plot=False, export=False):
    results = {"zoscLower": [], "zoscUpper": []}
    for _n_e in num_eve:
        _zosc_lower, _zosc_upper = zosc_rearrange(num_bob, _n_e, mu, num_steps)
        results["zoscLower"].append(_zosc_lower)
        results["zoscUpper"].append(_zosc_upper)
    results["eve"] = num_eve
    #print(results)
    if export:
        pd.DataFrame.from_dict(results).to_csv("zosr-rearrange-rayleigh-b{:d}-m{:.2f}.dat".format(num_bob, mu), sep='\t', index=False)
    if plot:
        plot_results(results)
    return results

def plot_results(results):
    fig, axs = plt.subplots()
    axs.plot(results["eve"], results["zoscLower"], 'o-', label="Lower Bound")
    axs.plot(results["eve"], results["zoscUpper"], '^--', label="Upper Bound")
    axs.set_xlabel("Number of Antennas at Eve $n_E$")
    axs.set_ylabel("Zero-Outage Secrecy Rate")
    axs.legend()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--num_bob", type=int, default=2)
    parser.add_argument("-e", "--num_eve", type=int, default=[1], nargs="+")
    parser.add_argument("-m", "--mu", type=float, default=.5)
    parser.add_argument("--num_steps", type=int, default=5000)
    parser.add_argument("--plot", action="store_true")
    parser.add_argument("--export", action="store_true")
    args = vars(parser.parse_args())
    main(**args)
    plt.show()
