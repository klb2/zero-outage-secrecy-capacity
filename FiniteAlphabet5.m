clear all;
close all;

N_vec     = 2:5; % from alphabet size 2 to 5
ZOSC      = zeros(1,length(N_vec));
p_Ind     = zeros(1,length(N_vec));

%%% The PMF to be solved
P2_solved = zeros(1,N_vec(1)^3);
P3_solved = zeros(1,N_vec(2)^3);
P4_solved = zeros(1,N_vec(end)^3);

Ind_2     = zeros(2^3, 3);
Ind_3     = zeros(3^3, 3);
Ind_4     = zeros(4^3, 3);

%%% Initialization of marginal probabilities
% alphabet size = 2
p_x1_0 = 0.2;
p_x1_1 = 1-p_x1_0;
p_x2_0 = 0.1;
p_x2_1 = 1-p_x2_0;
p_y_0  = 0.1;
p_y_1  = 1-p_y_0;
P2     = [p_x1_0 p_x1_1 p_x2_0 p_x2_1 p_y_0 p_y_1]';
% alphabet size = 3
p_x1_0 = 0.1;
p_x1_1 = 0.1;
p_x1_2 = 1-p_x1_0-p_x1_1;
p_x2_0 = 0.1;
p_x2_1 = 0.2;
p_x2_2 = 1-p_x2_0-p_x2_1;
p_y_0  = 0.87;
p_y_1  = 0.1;
p_y_2  = 1-p_y_0-p_y_1;
P3     = [p_x1_0 p_x1_1 p_x1_2 p_x2_0 p_x2_1 p_x2_2 p_y_0 p_y_1 p_y_2]';
% alphabet size = 4
p_x1_0 = 0.3;
p_x1_1 = 0.3;
p_x1_2 = 0.1;
p_x1_3 = 1-p_x1_0-p_x1_1-p_x1_2;
p_x2_0 = 0.2;
p_x2_1 = 0.2;
p_x2_2 = 0.1;
p_x2_3 = 1-p_x2_0-p_x2_1-p_x2_2;
p_y_0  = 0.73;
p_y_1  = 0.1;
p_y_2  = 0.1;
p_y_3  = 1-p_y_0-p_y_1-p_y_2;
P4     = [p_x1_0 p_x1_1 p_x1_2 p_x1_3 p_x2_0 p_x2_1 p_x2_2 p_x2_3 p_y_0 p_y_1 p_y_2 p_y_3]';
% alphabet size = 5
p_x1_0 = 0.3;
p_x1_1 = 0.3;
p_x1_2 = 0.1;
p_x1_3 = 0.1;
p_x1_4 = 1-p_x1_0-p_x1_1-p_x1_2-p_x1_3;
p_x2_0 = 0.2;
p_x2_1 = 0.2;
p_x2_2 = 0.1;
p_x2_3 = 0.05;
p_x2_4 = 1-p_x2_0-p_x2_1-p_x2_2-p_x2_3;
p_y_0  = 0.73;
p_y_1  = 0.1;
p_y_2  = 0.1;
p_y_3  = 0.04;
p_y_4  = 1-p_y_0-p_y_1-p_y_2-p_y_3;
P5     = [p_x1_0 p_x1_1 p_x1_2 p_x1_3 p_x1_4 p_x2_0 p_x2_1 p_x2_2 p_x2_3 p_x2_4  p_y_0 p_y_1 p_y_2 p_y_3 p_y_4]';

for N = N_vec
    
    p_vec = zeros(1,N^3);
    p     = zeros(1,N^3);
    Rs    = zeros(1,N^3);
    
    % initialization of the M-ary expnasion table, while fulfilling ZOSC>0
    for x1 = 0:N-1
        for x2 = 0:N-1
            for y = 0:N-1
                Ind = N^2*x1 + N*x2 + y + 1;
                if N==2
                    Ind_2(Ind,:)=[x1 x2 y];
                elseif N==3
                    Ind_3(Ind,:)=[x1 x2 y];
                elseif N==4
                    Ind_4(Ind,:)=[x1 x2 y];
                end
                if x1 + x2 > y
                    p_vec(Ind) = Ind;
                    p(Ind)  = 1;
                    Rs(Ind) = 1/2 *(log2(1+x1+x2) - log2(1+y));
                end
            end
        end
    end
    disp([num2str(sum(p)), ' variables, ', num2str(3*N+3), ' equations'])
    %plot(Rs)
    
    px1_vec = zeros(N,N^2);
    px2_vec = zeros(N,N^2);
    py_vec  = zeros(N,N^2);
    
    % A is the binary coefficient matrix such that A*p=P, i.e., to fulfill
    % the given marginal probabilities.
    A       = zeros(3*N, N^3);
    
    % Construct the binary matrix A from the non-zero p (somehow using the FFT structure)
    % The structure of A is illustrated as follows by the binary case:
    % the 1st row of A = P_{X1}(0), which is the marginalization of X2 and
    % Y;
    % the 2nd row of A = P_{X1}(1), which is the marginalization of X2 and
    % Y;
    % the 3rd row of A = P_{X2}(0), which is the marginalization of X1 and
    % Y;
    % the 4th row of A = P_{X2}(1), which is the marginalization of X1 and
    % Y;
    % the 5th row of A = P_{Y}(0), which is the marginalization of X1 and
    % X2;
    % the 6th row of A = P_{Y}(1), which is the marginalization of X1 and
    % X2;
    % only the indexed joint probabilities contritubting to the rows of A
    % will be '1', otherwise, it will be '0'.
    % Hence, A(1) = [0 0 1 0 0 0 0 0]
    %        A(2) = [0 0 0 0 1 0 1 1]    
    %        A(3) = [0 0 0 0 1 0 0 0]
    %        A(4) = [0 0 1 0 0 0 1 1]
    %        A(5) = [0 0 1 0 1 0 1 0]
    %        A(6) = [0 0 0 0 0 0 0 1]
    
    for x1 = 0:N-1
        px1 = 0;
        for x2 = 0:N-1 % the 2 inner-loop's are used to marginalize the joint PMF
            for y = 0:N-1
                Ind = N^2*x1 + N*x2 + y + 1; % generate the indices of the component of the PMF for marginalization
                px1 = [px1 p_vec(Ind)]; % use the indices to pick out the component variable from the list p_vec
            end
        end
        px1(1) = [];
        px1_vec(x1+1,:) = px1;
        A(x1+1, px1(find(px1>0)))=1; % only select the component with positive probability
    end
    
    for x2 = 0:N-1
        px2 = 0;
        for x1 = 0:N-1 % the 2 inner-loop's are used to marginalize the joint PMF
            for y = 0:N-1
                Ind = N^2*x1 + N*x2 + y + 1; % generate the indices of the component of the PMF for marginalization
                px2 = [px2 p_vec(Ind)]; % use the indices to pick out the component variable from the list p_vec
            end
        end
        px2(1) = [];
        px2_vec(x2+1,:) = px2;
        A(x1+1+x2+1, px2(find(px2>0)))=1; % only select the component with positive probability
    end
    
    for y = 0:N-1
        py = 0;
        for x1 = 0:N-1 % the 2 inner-loop's are used to marginalize the joint PMF
            for x2 = 0:N-1
                Ind = N^2*x1 + N*x2 + y + 1; % generate the indices of the component of the PMF for marginalization
                py = [py p_vec(Ind)]; % use the indices to pick out the component variable from the list p_vec
            end
        end
        py(1) = [];
        py_vec(y+1,:) = py;
        A(x1+1+x2+1+y+1, py(find(py>0)))=1; % only select the component with positive probability
    end
    
    num_var    = sum(p)-1;
    num_eq     = 3*N;
    Ind_pos_Rs = find(Rs>0);
    tmp_Rs     = Rs(Ind_pos_Rs);
    
    b = randn(1,3*N);
    I = eye(N^3);
    d = zeros(1,N^3);
    
    % Initialize the constant vector P for CVX
    if N==2
        P = P2;
    elseif N==3
        P = P3;
    elseif N==4
        P = P4;
    elseif N==5
        P = P5;
    else
    end
    
    % solve the optimization problem:
    % The necessary parts are in the constraints, including the linear
    % equation and the probability constraints
    cvx_begin
    variable p(N^3);
    minimize( sum(p) );
    subject to
    -I*p <= d';
    (diag(p))-I <= 0;
    ones(1,N^3)*p == 1;
    A*p == P;
    cvx_end
    
    if num_var <=num_eq % for the case N=2
        ZOSC(N-N_vec(1)+1) = min(tmp_Rs);
        if p(end)>0
            p_Ind = 8;
        elseif p(3)>0 || p(5)>0
            p_Ind = 3;
        else
            p_Ind = 7;
        end
        P2_solved = p;
    else % For the cases with N>2
        [m_val, m_ind] = min(tmp_Rs);
        tmp_Ind        = Ind_pos_Rs(m_ind);
        A(:,tmp_Ind)   = zeros(length(A(:,1)), 1); % set the whole column of A as 0,
        % while the rate calculated from the corresponding probability with the index the same as that of the column, is the smallest
        tmp_Rs(m_ind)  = 1000; % remove the minimum rate by setting it as a big value
        while num_var >=num_eq
            
            cvx_begin
            variable p(N^3);
            minimize( sum(p) );
            %minimize( 2 );
            subject to
            -I*p <= d';
            (diag(p))-I <= 0;
            A*p==P;
            ones(1,N^3)*p==1;
            cvx_end
            
            if strcmp(num2str(cvx_status),'Solved') % check if CVX can find a feasible solution or not
                [m_val, m_ind] = min(tmp_Rs);
                tmp_Ind        = Ind_pos_Rs(m_ind);
                A(:,tmp_Ind)   = zeros(length(A(:,1)), 1);
                tmp_Rs(m_ind)  = 1000;
                tmp_ZOSC_Ind   = find(p>10^(-9));
                tmp_Rs2 = Rs(tmp_ZOSC_Ind);
                [ZOSC_tmp ZOSC_Ind] = min(tmp_Rs2);
                p_Ind(N-N_vec(1)+1) = tmp_ZOSC_Ind(ZOSC_Ind); % find the index of the dominant combination
                % save the PMF
                if N==3
                    P3_solved = p;
                elseif N==4
                    P4_solved = p;
                end
                ZOSC(N-N_vec(1)+1) = ZOSC_tmp;
            else
                break
            end
            length(find(p>10^(-9)))
            A
            %
            num_var = num_var - length(m_ind);
            %         if num_var<num_eq
            %             ZOSC(N-N_vec(1)+1) = min(tmp_Rs);
            %         end
        end
%         if strcmp(num2str(cvx_status),'Solved')
%             ZOSC(N-N_vec(1)+1) = ZOSC_tmp;
%         else break
%         end
    end
    
    tt = 1;
end

ZOSC

plot(N_vec, ZOSC)
grid on

ylabel('ZOSC (bpcu)')
xlabel('Size of channel alphabet')
%title(['P1'' with ', '\eta_1=', num2str(eta1), ', \eta_2=', num2str(eta2), ', n_1=', num2str(n1), ', h_1=', num2str(h1), ', h_2=', num2str(h2), ', P_1=', num2str(P1), ', P_2=', num2str(P2)])
ax = gca;
ax.FontSize = 14;
ax.TitleFontWeight = 'normal';
set(gcf,'Position',[100 100 1000 700])
set(gca,'FontSize',16,'FontName','Times New Roman')