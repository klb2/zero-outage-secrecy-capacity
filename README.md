# Zero-Outage Secrecy-Capacity of Dependent Fading Wiretap Channels

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/klb2%2Fzero-outage-secrecy-capacity/master)

This repository contains supplementary material for the paper "On the
Zero-Outage Secrecy-Capacity of Dependent Fading Wiretap Channels" (Eduard A.
Jorswieck, Pin-Hsun Lin, and Karl-Ludwig Besser, Entropy, vol. 24, no. 1: 99,
Jan. 2022, [DOI:10.3390/e24010099](https://doi.org/10.3390/e24010099)).

The idea is to give an interactive version of the calculations and presented
concepts to the reader. One can also change different parameters and explore
different behaviors on their own.
You can also run the scripts to reproduce the results shown in the paper.


## File List
The following files are provided in this repository:

* [ZOSR - Rearrangement
  Algorithm.ipynb](https://mybinder.org/v2/gl/klb2%2Fzero-outage-secrecy-capacity/master?filepath=ZOSR%20%2D%20Rearrangement%20Algorithm.ipynb):
  Jupyter notebook that contains the calculation of the achievable ZOSR using
  the rearrangement algorithm.
* `run.sh`: Shell script to reproduce the results presented in the figures in
  the paper.
* `zosc_rearrange.py`: Python module that contains the calculations of the ZOSR
  using the rearrangement algorithm.
* `FiniteAlphabet5.m`: Matlab script to calculate the ZOSC of the example with
  a finite alphabet. (Requires [CVX](http://cvxr.com/cvx/))


## Usage
### Running it online
The easiest way is to use services like [Binder](https://mybinder.org/) to run
the notebook online. Simply navigate to
[https://mybinder.org/v2/gl/klb2%2Fzero-outage-secrecy-capacity/master](https://mybinder.org/v2/gl/klb2%2Fzero-outage-secrecy-capacity/master)
to run the notebooks in your browser without setting everything up locally.

### Local Installation
If you want to run it locally on your machine, Python3 and Jupyter are needed.
The present code was developed and tested with the following versions:
- Python 3.9
- Jupyter 1.0
- numpy 1.19
- scipy 1.6
- rearrangement_algorithm 0.1.1

Make sure you have [Python3](https://www.python.org/downloads/) installed on
your computer.
You can then install the required packages (including Jupyter) by running
```bash
pip3 install -r requirements.txt
jupyter nbextension enable --py widgetsnbextension
```
This will install all the needed packages which are listed in the requirements 
file. The second line enables the interactive controls in the Jupyter
notebooks.

Finally, you can run the Jupyter notebooks with
```bash
jupyter notebook 'ZOSR - Rearrangement Algorithm.ipynb'
```

For the Matlab script, you need Matlab and [CVX](http://cvxr.com/cvx/) to
reproduce the results.
The script was developed and tested with the following versions:
- Matlab 9.10 (R2021a)
- CVX Version 2.2


You can also recreate the figures from the paper by running
```bash
bash run.sh
```


## Acknowledgements
This research was supported in part by the Deutsche Forschungsgemeinschaft
(DFG) under grants JO 801/23-1 and JO 801/25-1.


## License and Referencing
This program is licensed under the GPLv3 license. If you in any way use this
code for research that results in publications, please cite our original
article listed above.

```bibtex
@article{Jorswieck2022zosc,
	author = {Jorswieck, Eduard A. and Lin, Pin-Hsun and Besser, Karl-Ludwig},
	title = {On the Zero-Outage Secrecy-Capacity of Dependent Fading Wiretap Channels},
	journal = {Entropy},
	publisher = {MDPI},
	volume = {24},
	number = {1},
	eid = {99},
	month = {1},
	year = {2022},
	doi = {10.3390/e24010099},
}
```
