#!/bin/sh

# This repository contains supplementary material for the paper "On the
# Zero-Outage Secrecy-Capacity of Dependent Fading Wiretap Channels" (Eduard A.
# Jorswieck, Pin-Hsun Lin, and Karl-Ludwig Besser, Entropy, vol. 24, no. 1: 99,
# Jan. 2022).
#
# Copyright (C) 20XX ...
# License: GPLv3

echo "Calcuate bounds on the achievable zero-outage secrecy rate..."
echo "n_B = 2"
echo "mu = 0.1"
python3 zosc_rearrange.py -b 2 -e {1..15} -m .1 --num_steps=10000 --export --plot
echo "mu = 0.2"
python3 zosc_rearrange.py -b 2 -e {1..15} -m .2 --num_steps=10000 --export --plot

echo "n_B = 4"
echo "mu = 0.1"
python3 zosc_rearrange.py -b 4 -e {1..15} -m .1 --num_steps=10000 --export --plot
echo "mu = 0.2"
python3 zosc_rearrange.py -b 4 -e {1..15} -m .2 --num_steps=10000 --export --plot

echo "Calculate the zero-outage secrecy capacity for the example with a finite alphabet..."
matlab -nodisplay -nosplash -nodesktop -r "run('FiniteAlphabet5.m'); exit;"
